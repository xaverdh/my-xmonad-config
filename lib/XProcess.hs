{-# language ScopedTypeVariables, FlexibleInstances #-}
{-# language DeriveFunctor #-}
module XProcess where

import XMonad
import qualified System.Process as Proc
import System.IO
import Data.Bifunctor

newtype Out a = Out { getStdout :: a } deriving Functor
newtype Err a = Err { getStderr :: a } deriving Functor


class CallProcessRet r where
  fromProcessCallRet :: Out String -> Err String -> r

instance CallProcessRet (Out String, Err String) where
  fromProcessCallRet = (,)

instance CallProcessRet (Err String, Out String) where
  fromProcessCallRet = flip (,)

instance CallProcessRet (Out String) where
  fromProcessCallRet = const

instance CallProcessRet (Err String) where
  fromProcessCallRet = flip const

instance CallProcessRet () where
  fromProcessCallRet = const $ const ()

instance CallProcessRet String where
  fromProcessCallRet = const . getStdout



call :: (MonadIO io,CallProcessRet r)
  => FilePath -> [String] -> String -> io r
call path args input =
  uncurry fromProcessCallRet . bimap Out Err
  <$> readProcessX path args input

readProcessX :: MonadIO io
  => FilePath 
  -> [String] 
  -> String 
  -> io (String,String)
readProcessX p args s = liftIO $ do
  (Just hin,Just hout,Just herr,_) <- Proc.createProcess
    $ (Proc.proc p args)
      { Proc.std_in = Proc.CreatePipe
      , Proc.std_out = Proc.CreatePipe
      , Proc.std_err = Proc.CreatePipe }
  hPutStr hin s
  hClose hin
  out <- hGetContents hout
  length out `seq` hClose hout
  err <- hGetContents herr
  length err `seq` hClose herr
  return (out,err)

