{-# language TupleSections, LambdaCase #-}
module XKeyBindings 
  ( mkBindings )
where

import XMonad
import XMonad.Util.Run
-- import XMonad.Prompt
import XMonad.Actions.WindowBringer (gotoMenuArgs,bringMenuArgs)
import XMonad.Util.Paste (pasteSelection)
import XMonad.Layout.Minimize
import XMonad.Actions.Minimize

import Control.Monad.Extra
import qualified Data.String as S
import System.Environment (lookupEnv,getArgs)
import System.Info (arch,os)
import System.FilePath ((</>))
import System.Posix.Process (executeFile)

import XControl
import Utils
import Oneko


mkBindings :: [ ( (KeyMask, KeySym), X () ) ]
mkBindings =
  [ mod xK_p ~> dmenuRun

  , ctrl xK_p ~> dmenuNixRun

  , ctrl xK_q ~> withFocused killWindow

  , nomod xK_Insert ~> pasteSelection

  , mod xK_q ~> restartXMonad

  , mod xK_r ~> compileRestart

  , mod xK_i ~> spawn "xsystem-info"

  , mod xK_g ~> gotoMenuArgs
    ( dmenuNoCase <> dmenuLines 20 )
  , mod xK_b ~> bringMenuArgs
    ( dmenuNoCase <> dmenuLines 20 )

  , mod xK_o ~> oneko
  , modShift xK_o ~> safeSpawn "killall" ["oneko"]

  , mod xK_m ~> withFocused minimizeWindow
  , modShift xK_m ~> withLastMinimized maximizeWindowAndFocus
  , mod xK_0 ~> showCurrentWorkspace 0

  , mod xK_F1 ~> blankScreen

  , nomod xf86K_AudioPlay ~> safeSpawn "playerctl" [ "play-pause" ]

  , nomod xf86K_AudioMute ~> toggleAudio

  , nomod xf86K_AudioLowerVolume ~> decVol 5

  , nomod xf86K_AudioRaiseVolume ~> incVol 5

  , nomod xf86K_MonBrightnessDown ~> decLight 5

  , nomod xf86K_MonBrightnessUp ~> incLight 5

  , mod xK_F7 ~> safeSpawn "xrandr" [ "--auto" ]
  , nomod xf86K_Display ~> safeSpawn "xrandr" [ "--auto" ] ]

  where
    ctrl = (controlMask,)
    mod = (mod4Mask,)
    nomod = (noModMask,)
    modShift = (mod4Mask .|. shiftMask,)

    infixr 2 ~>
    (~>) :: (KeyMask, KeySym) -> X ()
         -> ( (KeyMask, KeySym), X () )
    (~>) = (,)

restartXMonad :: X ()
restartXMonad = restart "xmonad" True

compileRestart :: X ()
compileRestart = do
  dirs <- asks directories
  whenX (recompile dirs True) $ do
    writeStateToFile
    catchIO $ do
      args <- getArgs
      executeFile (cacheDir dirs </> compiledConfig) False args Nothing
  where
    compiledConfig = "xmonad-" <> arch <> "-" <> os

dmenuNixRun :: X ()
dmenuNixRun = dmenuExec ( dmenuNoCase <> dmenuPrompt "nix run")
  $ \p args -> safeSpawn "nix"
    ([ "run", "nixpkgs#" <> p ] <> args)

dmenuRun :: X ()
dmenuRun = dmenuExec ( dmenuNoCase <> dmenuPrompt "run") safeSpawn


dmenuLines :: Int -> [String]
dmenuLines n = ["-l", show n]

dmenuNoCase :: [String]
dmenuNoCase = ["-i"]

dmenuPrompt :: String -> [String]
dmenuPrompt p = ["-p",p]

dmenuExec :: [String] -> (String -> [String] -> X ()) -> X ()
dmenuExec dmenuArgs action = do
  opts <- runProcessWithInput "dmenu_path" [] ""
  cmd <- runProcessWithInput "dmenu" dmenuArgs opts
  case filter (/="") $ S.lines cmd of
    [] -> pure ()
    p:args -> action p args



showShellResult :: String -> String -> IO ()
showShellResult input output = pure ()

{- Additional Keysymbols -}

xf86K_AudioMute = 0x1008ff12
xf86K_AudioLowerVolume = 0x1008ff11
xf86K_AudioRaiseVolume = 0x1008ff13
xf86K_MonBrightnessDown = 0x1008ff03
xf86K_MonBrightnessUp = 0x1008ff02
xf86K_Display = 0x1008ff59
xf86K_AudioPlay = 0x1008ff14
