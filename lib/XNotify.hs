{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module XNotify where

import XMonad
import qualified XMonad.Util.ExtensibleState as XS
import qualified Libnotify as N
import qualified Data.Map as M
import Control.Monad.Extra

import Data.Typeable
import Data.String (IsString)


class (Typeable t,Ord t) => TokenSelector t

data TkFresh = TkFresh deriving (Eq,Ord)
instance TokenSelector TkFresh

newtype Icon = Icon { iconPath :: String }
  deriving (Eq,Ord,Show,IsString)

type MNotification = N.Mod N.Notification

class Monad m => HasNotifications m where
  notify :: TokenSelector t => t -> MNotification -> m ()
  freshNotify :: MNotification -> m ()
  freshNotify = notify TkFresh

newtype Tokens t = Tokens
  { tokenMap :: M.Map t N.Notification }
  deriving (Semigroup,Monoid,Typeable)

instance (Typeable i,Ord i) => ExtensionClass (Tokens i) where
  initialValue = mempty


notifyXRaw :: TokenSelector t
  => Maybe t
  -> ( Maybe N.Notification -> X MNotification )
  -> X ()
notifyXRaw msel f
  | Nothing <- msel =
    f Nothing >>= void . liftIO . N.display
  | Just sel <- msel = do
    modn <- f =<< XS.gets ( M.lookup sel . tokenMap )
    n' <- liftIO $ N.display modn
    XS.modify $ Tokens . M.insert sel n' . tokenMap

notifyX :: TokenSelector t => t -> MNotification -> X ()
notifyX sel n = notifyXRaw (Just sel) $ \n' ->
  pure $ n <> maybe mempty N.reuse n'

freshNotifyX :: MNotification -> X ()
freshNotifyX n = notifyXRaw
  ( Nothing :: Maybe TkFresh ) $ const (pure n)

notifyIO :: TokenSelector t => t -> MNotification -> IO ()
notifyIO _ = void . N.display

instance HasNotifications X where
  notify = notifyX
  freshNotify = freshNotifyX

instance HasNotifications IO where
  notify = notifyIO


mkBody :: String -> String -> String -> MNotification
mkBody name col v = N.body
  $ name <> ": <span foreground = \""
  <> col <> "\">" <> v <> "</span>"

icon :: Icon -> MNotification
icon = N.icon . iconPath



