{-# LANGUAGE OverloadedStrings, LambdaCase #-}
module XControl where

import XMonad
import XMonad.Util.Run

import Control.Monad.Extra
import Data.Function
import Data.String

import XNotify
import Utils
import System.Exit
import System.Posix.Process (ProcessStatus(..))

audioDown :: Icon
audioDown = "audio-volume-low"

audioOn :: Icon
audioOn = "audio-volume-medium"

audioUp :: Icon
audioUp = "audio-volume-high"

audioMuted :: Icon
audioMuted = "audio-volume-muted"

lightUp :: Icon
lightUp = "weather-clear"

lightDown :: Icon
lightDown = "weather-few-clouds-night"


data TkAudio = TkAudio deriving (Eq,Ord)
instance TokenSelector TkAudio

data TkLight = TkLight deriving (Eq,Ord)
instance TokenSelector TkLight

data TkPrompt = TkPrompt deriving (Eq,Ord)
instance TokenSelector TkPrompt

data TkWorkspaceId = TkWorkspaceId deriving (Eq,Ord)
instance TokenSelector  TkWorkspaceId

lightCtrl :: MonadIO io => [String] -> io String
lightCtrl args = runProcessWithInput "light" args ""

audioCtrl :: MonadIO io => [String] -> io String
audioCtrl args = runProcessWithInput "ponymix" args ""

incVol :: Int -> X ()
incVol v = do
  cur <- audioCtrl ["increase",show v]
  notify TkAudio ( mkBody "Vol" "cyan" cur
                  <> icon audioUp )

decVol :: Int -> X ()
decVol v = do
  cur <- audioCtrl ["decrease",show v]
  notify TkAudio ( mkBody "Vol" "cyan" cur
                  <> icon audioDown )

incLight :: Int -> X ()
incLight v = do
  lightCtrl ["-A",show v]
  cur <- lightCtrl ["-G"]
  notify TkLight ( mkBody "Brightness" "yellow" cur
                  <> icon lightUp )

decLight :: Int -> X ()
decLight v = do
  lightCtrl ["-U",show v]
  cur <- lightCtrl ["-G"]
  notify TkLight ( mkBody "Brightness" "yellow" cur
                  <> icon lightDown )

toggleAudio :: X ()
toggleAudio = void $ audioCtrl ["toggle"]

blankScreen :: X ()
blankScreen = void . sequence . replicate 2
  --   We have to blank twice. For some reason my screen
  --   wakes immediately after the first blanking attempt.
  --   The second attempt then works...
  $ runProcessWithInput
    "xset" [ "dpms", "force", "off" ] ""

toggleLight :: X ()
toggleLight = forked $ do
  l <- read <$> lightCtrl ["-b","-G"]
  case l of
    0 -> do
      lightCtrl ["-b","-I"]
      lightCtrl ["-c","-S","2"]
    _ -> do
      lightCtrl ["-b","-O"]
      lightCtrl ["-c","-S","0"]
      lightCtrl ["-b","-S","0"]

showCurrentWorkspace :: ScreenId -> X ()
showCurrentWorkspace sid =
  whenJustM (screenWorkspace sid)
  $ \wid -> do
    notify TkWorkspaceId ( mkBody "WorkspaceId" "yellow" wid )


