{-# language ScopedTypeVariables #-}
module Oneko
  ( oneko )
where

import XMonad
import XMonad.Util.Run (safeSpawn)
import System.Random (randomIO)

oneko :: X ()
oneko = do
  i :: Int <- liftIO randomIO
  safeSpawn "oneko"
    [ "-tora"
     ,"-fg" , color i
     ,"-bg" , "black"
     ,"-speed" , show $ speed i]
  where
    color j = case (j `div` 2) `mod` 5 of
      0 -> "white"
      1 -> "yellow"
      2 -> "green"
      3 -> "blue"
      4 -> "red"
    speed j = 16 - 5 + j `mod` 10

