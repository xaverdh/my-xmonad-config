module Utils
  ( (<&>)
  , spawnSafePID
  , spawnWithStatus
  , forked )
where

import XMonad

import Data.Bool
import Control.Concurrent (forkIO)
import Control.Monad.IO.Class
import Control.Monad.Extra (void)

import System.Posix.Process (executeFile,getProcessStatus,ProcessStatus)
import System.Posix.Types (ProcessID)

(<&>) :: Functor f => f a -> (a -> b) -> f b
(<&>) = flip fmap

spawnSafePID :: MonadIO m => String -> [String] -> m ProcessID
spawnSafePID prog args = xfork $ executeFile prog False args Nothing

spawnWithStatus :: MonadIO m => String -> [String] -> m (Maybe ProcessStatus)
spawnWithStatus prog args = do
  pid <- spawnSafePID prog args
  io $ getProcessStatus True True pid

forked :: MonadIO io => IO a -> io ()
forked = liftIO . void . forkIO . void

