
import XMonad hiding (ifM)
import XMonad.Util.EZConfig (additionalKeys,removeKeys)
import XMonad.Util.Run
import XMonad.Util.SessionStart
import XMonad.Hooks.SetWMName (setWMName)
import XMonad.Layout.Minimize
import XMonad.Layout.NoBorders
import XMonad.Operations (windows)
import XMonad.StackSet (greedyView)
import XMonad.ManageHook (doFloat,(-->),(=?),stringProperty)

import Data.Default
import Data.Foldable (asum)
import Data.Maybe
import System.Directory (getHomeDirectory,listDirectory)
import System.FilePath ((</>),(<.>))
import System.Posix.Files (getFileStatus,isRegularFile)
import System.Environment (setEnv,lookupEnv)
import Control.Monad
import Control.Monad.Extra

import XProcess
import XKeyBindings
import XNotify

data TkStartup = TkStartup deriving (Eq,Ord)
instance TokenSelector TkStartup


main :: IO ()
main = do
  dirs <- getDirectories
  flip launch dirs $ def
    { modMask = mod4Mask -- the "Win"-key
    , terminal = "terminal"
    , normalBorderColor = "black"
    , focusedBorderColor = "white"
    , startupHook = startup
    , manageHook = myManageHook
    , layoutHook = myLayoutHook }
    `removeKeys` [(mod4Mask,xK_q)]
    `additionalKeys` mkBindings

myLayoutHook = minimize (Tall 1 (3/100) (1/2)) ||| smartBorders Full


myManageHook :: ManageHook
myManageHook =
  stringProperty "_NET_WM_WINDOW_TYPE(ATOM)" =? "_NET_WM_WINDOW_TYPE_DIALOG" --> doFloat

startup :: X ()
startup = 
  ifM isSessionStart
    ( onInitial
      *> setSessionStarted )
    onRestart
  *> onStart

onInitial :: X ()
onInitial = do
  rescreen
  setNonReparenting
  whenJustM staticImage setBg
  switchToWorkspace 1
  notify TkStartup (mkBody "xmonad" "green" "started")

onRestart :: X ()
onRestart = do
  whenJustM staticImage setBg
  notify TkStartup (mkBody "xmonad" "yellow" "restarted")

onStart :: X ()
onStart = pure ()

switchToWorkspace :: Int -> X ()
switchToWorkspace i = 
  windows $ greedyView (show i)

setBg :: FilePath -> X ()
setBg image = do
  safeSpawn "feh" ["--no-fehbg","--bg-fill",image]

staticImage :: X (Maybe FilePath)
staticImage = liftIO $ do
  dir <- (</> ".background/") <$> getHomeDirectory
  content <- map (dir </>) <$> listDirectory dir
  fmap asum . forM content $ \path -> do
    stat <- getFileStatus path
    pure $ if isRegularFile stat then Just path else Nothing

setNonReparenting :: X ()
setNonReparenting = do
  liftIO $ setEnv "_JAVA_AWT_WM_NONREPARENTING" ""
  setWMName "LG3D"
  

